# Импортирую функцию суммирования
from Summator_module import appender

count = list()
# Бесконечный цикл - ввод числа. Выход - ввод q
while True:
    f = str(input('Введите целое число для суммирования, для окончания введите q: '))
    if 'q' in f:
        break
    # Добавление введенного числа в список, проверка на - целое ли число
    try:
        count.append(int(f))
    except ValueError:
        print('Можно вводить только целые числа')

# Вызов функции суммирования и передача списка аргументом
appender(*count)

print(count)

print(appender)